# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.1] - 2019-06-19

### Fixed

- Closed connection usage

## [1.0.0] - 2019-03-03

### Changed

- Migrate to `rethinkdb >= 2.4.0` usage

## [0.3.2] - 2018-07-02

### Added

- Methods to work with looped connections

## [0.3.1] - 2018-05-30

### Changed

- Wait for connection logic, more attempts and exponentail waiting
- Default pool size not is 10

### Fixed

- Pool release logic

## [0.3.0] - 2018-05-11

### Added

- Ability to wait for connection (currently fixed)
- Error when Pool if full

## [0.2.0]

### Added

- MIT License
- Some badges to readme

## Changed

- Rename package with '-'

### Fixed

- Mypy problems with variable types

[0.3.2]: hthttps://gitlab.com/AnjiProject/async-repool/compare/v0.3.1...v0.3.2
[0.3.1]: hthttps://gitlab.com/AnjiProject/async-repool/compare/v0.3.0...v0.3.1
[0.3.0]: hthttps://gitlab.com/AnjiProject/async-repool/compare/v0.2.0...v0.3.0
[0.2.0]: hthttps://gitlab.com/AnjiProject/async-repool/compare/v0.1.0...v0.2.0
