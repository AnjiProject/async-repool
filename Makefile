lint:
	pylint async_repool
	mypy --ignore-missing-imports async_repool
	pycodestyle async_repool
inspect:
	pipenv check
	bandit -r async_repool
pytest:
	pytest